class MachineACafe:

    total_money = 0
    coffees_nb = 0
    cups_nb = 0
    sugar_nb = 0
    has_coffee = True
    has_cups = True
    has_water = True
    has_sugar = True

    def insert_money(self, money):
        self.total_money += money

    def give_money_back(self, money):
        # retire l'argent au compteur total
        self.total_money -= money

    def pour_coffee(self, with_sugar):
        if with_sugar:
            self.sugar_nb -= 1
        self.coffees_nb += 1
        self.cups_nb -= 1


