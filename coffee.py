from math import floor


class CoffeeMachine:
    total_money = 0
    stock_of_coffee = 30
    # stock actuel de gobelet
    stock_of_cup = 50
    # stock max de gobelet
    nb_cups_max = 50
    stock_of_sugar = 40
    number_of_sugar = 0
    has_coffee = True
    has_cups = True
    has_water = True
    has_sugar = True
    has_mug = False
    max_coins = 10
    inserted_coins = 0
    cancel_button = False
    nb_coffee_max = 30

    def calcul_nb_coffees_to_pour(self, money):
        if 10 >= self.inserted_coins > 0 and money >= 40:
            return floor(money / 40)
        else:
            return 0

    def insert_money(self, money):
        self.total_money += money
        coffees_to_pour = self.calcul_nb_coffees_to_pour(money)
        i = 0
        while i < coffees_to_pour != 0:
            if money >= 40 and self.has_water and self.stock_of_coffee > 0:
                if self.has_mug:
                    self.sugar()
                    self.coffee()
                elif self.stock_of_cup > 0:
                    self.cup()
                    self.sugar()
                    self.coffee()
                else:
                    self.give_money_back(money / coffees_to_pour)
            else:
                self.give_money_back(money / coffees_to_pour)
            i += 1
        if coffees_to_pour == 0:
            self.give_money_back(money)

    def give_money_back(self, money):
        # retire l'argent au compteur total
        print(money)
        self.total_money -= money

    def coffee(self):
        self.stock_of_coffee -= 1

    def cup(self):
        self.stock_of_cup -= 1

    def sugar(self):
        self.stock_of_sugar -= self.number_of_sugar

    def add_cups(self, nb_cups):
        self.stock_of_cup += nb_cups

    def add_coffee(self, nb_coffees):
        self.stock_of_coffee += nb_coffees
