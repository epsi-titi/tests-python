import unittest
import coffee


# TODO vérifier tous les insert money
class MyTestCase(unittest.TestCase):
    price_of_coffee = 40

    # QUAND on met 40cts ALORS on décrémente de 1 le nombre de gobelet
    def test_coffee(self):
        # création d'une nouvelle machine
        machine = coffee.CoffeeMachine()
        initial_coffees_num = machine.stock_of_coffee
        machine.inserted_coins = 1
        # on insert de l'argent
        machine.insert_money(self.price_of_coffee)
        # comparaison entre le nombre de cafés d'avant et le nouveau
        self.assertEqual(initial_coffees_num - 1, machine.stock_of_coffee)

    # ETANT DONNE qu'il n'y a plus de café
    # QUAND on met 40cts
    # ALORS l'argent est rendu
    def test_no_coffee(self):
        # creation d'une machine à café
        machine = coffee.CoffeeMachine()
        machine.inserted_coins = 1
        # plus de café
        machine.stock_of_coffee = 0
        # insertion des 40 centimes
        machine.insert_money(self.price_of_coffee)
        # comparaison entre la quantité de café d'avant et la nouvelle
        self.assertEqual(0, machine.stock_of_coffee)
        # coparaison entre l'argent rendue et l'argent total dans la machine
        self.assertEqual(0, machine.total_money)

    #  QUAND le technicien remet <x> gobelets on ajoute <x> au stock de gobelets
    def test_no_cup(self):
        # creation d'une machine à café
        machine = coffee.CoffeeMachine()
        machine.inserted_coins = 1
        # plus de gobelet
        machine.stock_of_cup = 0
        # insertion des 40 centimes
        machine.insert_money(self.price_of_coffee)
        # comparaison entre le nombre de gobelets d'avant et le nouveau
        self.assertEqual(0, machine.stock_of_cup)
        # coparaison entre l'argent rendue et l'argent total dans la machine
        self.assertEqual(0, machine.total_money)

    # Etant donné : que la machine est hors service (plus d’eau)
    # Quand: on met 40 centimes
    # Alors: on rend la monnaie
    def test_no_water(self):
        # creation d'une machine à café
        machine = coffee.CoffeeMachine()
        machine.inserted_coins = 1
        # plus d'eau ...
        machine.has_water = False
        # insertion des 40 centimes
        machine.insert_money(self.price_of_coffee)
        # coparaison entre l'argent rendue et l'argent total dans la machine
        self.assertEqual(0, machine.total_money)
        # comparaison entre la quantité d'eau d'avant et la nouvelle
        self.assertEqual(False, machine.has_water)

    # Etant donné que l'utilisateur a appuyé sur sucre
    # Quand l'utilisateur met 40 cts Alors le café coule avec le sucre ajouté
    def test_sugar_added(self):
        # creation d'une machine à café
        machine = coffee.CoffeeMachine()
        machine.inserted_coins = 1
        # Variable sucre dans la machine
        initialSugar = machine.stock_of_sugar
        machine.number_of_sugar = 1
        # Variable cafe dans la machine
        initialCoffee = machine.stock_of_coffee
        # insertion des 40 centimes
        machine.insert_money(self.price_of_coffee)
        # comparaison entre les sucres
        self.assertEqual(initialSugar - 1, machine.stock_of_sugar)
        # comparaison entre le nombre de cafe initial et le nouveau
        self.assertEqual(initialCoffee - 1, machine.stock_of_coffee)

    # QUAND on met 40cts ALORS on décrémente de 1 le nombre de gobelet
    def test_decrease_cup(self):
        # creation d'une machine à café
        machine = coffee.CoffeeMachine()
        machine.inserted_coins = 1
        # isolation du nombre de gobelets dans une variable
        initial_cups_nb = machine.stock_of_cup
        # insertion des 40 centimes
        machine.insert_money(self.price_of_coffee)
        # comparaison sur le nombre de gobelets
        self.assertEqual(initial_cups_nb - 1, machine.stock_of_cup)

    # QUAND un café coule ALORS on décrémente de 1 le nombre de doses de café
    def test_decrease_coffee_dose(self):
        # creation d'une machine à café
        machine = coffee.CoffeeMachine()
        machine.inserted_coins = 1
        # isolation du nombre de cafe dans une variable
        actual_dose_nb = machine.stock_of_coffee
        # insertion des 40 centimes
        machine.insert_money(self.price_of_coffee)
        # comparaison sur le nombre de dose de cafe
        self.assertEqual(actual_dose_nb - 1, machine.stock_of_coffee)

    # ETANT donne qu'il n'y a plus de gobelets
    # ET qu'une tasse est détecté et QUAND on met 40cts
    # ALORS le café coule
    def test_no_cups_and_mug(self):
        # creation d'une machine à café
        machine = coffee.CoffeeMachine()
        machine.inserted_coins = 1
        # isolation du nombre de cafe dans une variable
        actual_dose_nb = machine.stock_of_coffee
        # il y a plus de cup
        machine.stock_of_cup = 0
        # ajout de la tasse
        machine.has_mug = True
        # insertion des 40 centimes
        machine.insert_money(self.price_of_coffee)
        self.assertEqual(actual_dose_nb - 1, machine.stock_of_coffee)

    # QUAND on met 4 pièces pour un total de moins de 40 cts
    # ALORS pas de café + rend les pièces
    def test_max_pieces_less_money(self):
        inserted_money = 20
        # récupération de la machine à café
        machine = coffee.CoffeeMachine()
        machine.inserted_coins = 4
        # insertion de la monnaie
        machine.insert_money(inserted_money)
        # récupération de la monnaie présent dans la machine
        machine_total_money = machine.total_money
        self.assertEqual(0, machine_total_money)

    # Étant donné : qu'on met une tasse dans la machine
    # Quand : on met 40 cts
    # Alors : le café coule sans donner de gobelet
    def test_cafe_with_tasse(self):
        # creation d'une machine à café
        machine = coffee.CoffeeMachine()
        machine.inserted_coins = 1
        # isolation du nombre de café dans une variable
        initial_coffees_num = machine.stock_of_coffee
        # isolation du nombre de gobelet dans une variable
        initial_cup_num = machine.stock_of_cup
        # ajout de la tasse
        machine.has_mug = True
        # insertion des 40 centimes
        machine.insert_money(self.price_of_coffee)
        # comparaison sur le nombre de cafe
        self.assertEqual(initial_coffees_num - 1, machine.stock_of_coffee)
        # comparaison sur le nombre de gobelet
        self.assertEqual(initial_cup_num, machine.stock_of_cup)

    # QUAND le technicien remet <x> gobelets on ajoute <x> au stock de gobelets
    def test_add_cups(self):
        # récupération de la machine à café
        machine = coffee.CoffeeMachine()
        machine.stock_of_cup = 20
        # récupération du nombre de gobelets dans la machine
        nb_cups = machine.stock_of_cup
        # l'agent remet des gobelets
        machine.add_cups(machine.nb_cups_max - nb_cups)
        # vérifie que la machine a le nb maximum de gobelets
        self.assertEqual(machine.stock_of_cup, machine.nb_cups_max)

    # ETANT donné qu'il n'y a qu'un seul gobelet dans la machine
    # QUAND on met 40cts deux fois ALORS on obtient un
    # seul café ET 40cts car plus de gobelet
    def test_one_cup_two_coffees_requested(self):
        # les 80 centimes
        money = 80
        # creation d'une machine à café
        machine = coffee.CoffeeMachine()
        machine.inserted_coins = 1
        # la machine n'a qu'un gobelet
        machine.stock_of_cup = 1
        # on prend la monnaie
        machine.insert_money(80)
        # test sur le nombre de gobelets et sur la monnaie restante
        self.assertEqual(machine.stock_of_cup, 0)
        self.assertEqual(machine.total_money, 40)

    # QUAND on met 40 cts ET 50cts
    # ALORS deux cafés coulent et 90cts sont encaissés.
    def test_insert_two_money(self):
        # les deux pièces
        money1 = 40
        money2 = 50
        given_money = money1 + money2
        # on initialise la machine à café
        machine = coffee.CoffeeMachine()
        machine.inserted_coins = 3
        # on prend la monnaie
        machine.insert_money(given_money)
        # on verifie que les deux monaies sont supérieur ou égales à 40
        initial_money = machine.total_money
        self.assertEqual(initial_money, given_money)

    # Étant donné que le café n’est pas coulé et que 40cts ont été insérés
    # Quand le client appuie sur annuler  Alors
    # le paiement est remboursé et aucun café n’est servie
    def test_cancel_coffee_give_money_back(self):
        # argent inséré
        given_money = 40
        # initialisation de la machine a café
        machine = coffee.CoffeeMachine()
        machine.cancel_button = True
        # on rembourse
        machine.give_money_back(given_money)
        self.assertGreaterEqual(40, given_money)

    # QUAND le total de <x> pièces est supérieur à 40 centimes
    # ALORS la machine fait couler un café et encaisse la monnaie CAS 1 ... 4
    def test_four_coffees(self):
        # argent inséré
        given_money = 160
        # initialisation de la machine a café
        machine = coffee.CoffeeMachine()
        machine.inserted_coins = 8
        machine.insert_money(given_money)
        self.assertEqual(machine.stock_of_coffee, 26)
        self.assertEqual(machine.total_money, 160)

    # ETANT DONNE que le stock de café est de 30 - <x> doses
    # QUAND on ajoute <x> + 1 dose ALORS le stock de café est de 30 doses CAS 0, 1, 30
    def test_add_coffees_doses(self):
        # récupération de la machine à café
        machine = coffee.CoffeeMachine()
        machine.stock_of_coffee = 20
        # récupération du nombre de cafe dans la machine
        nb_coffees = machine.stock_of_coffee
        # l'agent remet des doses de cafés
        machine.add_coffee(machine.nb_coffee_max - nb_coffees)
        # vérifie que la machine a le nb maximum de café
        self.assertEqual(machine.stock_of_coffee, machine.nb_coffee_max)


if __name__ == '__main__':
    unittest.main()
